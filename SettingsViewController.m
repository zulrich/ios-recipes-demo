//
//  SettingsViewController.m
//  Recipes
//
//  Created by Zack Ulrich on 9/23/15.
//
//

#import "SettingsViewController.h"
#import "RecipeListTableViewController.h"
@interface SettingsViewController ()
@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;

@property (nonatomic, strong) NSString *username;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    self.userNameLbl.text = self.username;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signOut:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
    self.userNameLbl.text = nil;
    self.username = nil;
    [self.tabBarController setSelectedIndex:0];
        
}
@end
