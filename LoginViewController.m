//
//  LoginViewController.m
//  Recipes
//
//  Created by Zack Ulrich on 9/23/15.
//
//

#import "LoginViewController.h"

@interface LoginViewController ()
@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic, strong) NSString *username;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)doLogin:(id)sender {
    
    [self loginComplete];
    
}

-(void)loginComplete{
    [[NSUserDefaults standardUserDefaults] setObject:self.usernameField.text forKey:@"username"];
    
    self.username = self.usernameField.text;
    
    [self performSegueWithIdentifier:@"startSegue" sender:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
